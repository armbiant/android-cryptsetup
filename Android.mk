#
# In order for cryptsetup to be useful, the kernel config needs
# CONFIG_CRYPTO_USER_API_SKCIPHER=y set. To keep security attack
# surface minimal, should only be performed on a development sandbox
# build of the kernel.
#

LOCAL_PATH := $(call my-dir)

libcryptsetup_c_includes := $(LOCAL_PATH)/lib \
                            $(LOCAL_PATH)/lib/crypto_backend \
                            $(LOCAL_PATH)/lib/loopaes \
                            $(LOCAL_PATH)/lib/luks1 \
                            $(LOCAL_PATH)/lib/tcrypt \
                            $(LOCAL_PATH)/lib/verity

libcryptsetup_cflags := -include android_config.h -include openssl/ssl.h

cryptsetup_src_files := src/cryptsetup.c \
                        src/utils_password.c \
                        src/utils_tools.c

libcryptsetup_src_files := lib/crypt_plain.c \
                           lib/libdevmapper.c \
                           lib/random.c \
                           lib/setup.c \
                           lib/utils.c \
                           lib/utils_benchmark.c \
                           lib/utils_crypt.c \
                           lib/utils_device.c \
                           lib/utils_devpath.c \
                           lib/utils_fips.c \
                           lib/utils_loop.c \
                           lib/utils_wipe.c \
                           lib/volumekey.c \
                           lib/crypto_backend/crc32.c \
                           lib/crypto_backend/crypto_cipher_kernel.c \
                           lib/crypto_backend/crypto_openssl.c \
                           lib/crypto_backend/crypto_storage.c \
                           lib/crypto_backend/pbkdf_check.c \
                           lib/loopaes/loopaes.c \
                           lib/luks1/af.c \
                           lib/luks1/keymanage.c \
                           lib/luks1/keyencryption.c \
                           lib/tcrypt/tcrypt.c \
                           lib/verity/verity.c \
                           lib/verity/verity_hash.c

popt_src_files := android_popt.cpp # more or less copy from gptfdisk

include $(CLEAR_VARS)
LOCAL_MODULE := cryptsetup
LOCAL_CFLAGS := -O2 -g
LOCAL_CFLAGS += $(libcryptsetup_cflags)
LOCAL_MODULE_TAGS := eng
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)

# command and required library modules files, directly compile all
LOCAL_SRC_FILES := $(cryptsetup_src_files) \
                   $(popt_src_files) \
                   $(libcryptsetup_src_files)

LOCAL_C_INCLUDES := $(libcryptsetup_c_includes)

LOCAL_STATIC_LIBRARIES := libdevmapper # because we are the only user
LOCAL_SHARED_LIBRARIES := libext2_uuid libcrypto libselinux

include $(BUILD_EXECUTABLE)
